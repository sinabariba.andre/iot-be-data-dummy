const express = require("express");
const morgan = require("morgan");
const app = express();
const db = require("./models/index");
const PORT = process.env.PORT || 3000;

app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// ROutes

const apiRoutes = require("./routes/api-tes");

app.use("/api/v/", apiRoutes);

app.use((req, res, next) => {
  const error = new Error("NOT FOUND");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message,
    },
  });
});

app.listen(PORT, () => {
  console.log(`listening port ${PORT}`);
});
