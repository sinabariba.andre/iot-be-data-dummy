"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const randomNum = () => Math.floor(Math.random() * 200);
    return queryInterface.bulkInsert("parameters", [
      {
        parameter: "Debit",
        range: randomNum(),
        unit: "01",
        satuan: "m3/s",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parameter: "TSS",
        range: randomNum(),
        unit: "01",
        satuan: "mg/L",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parameter: "pH",
        range: randomNum(),
        unit: "01",
        satuan: "mg/L",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parameter: "NO3N",
        range: randomNum(),
        unit: "01",
        satuan: "mg/L",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parameter: "PO4",
        range: randomNum(),
        unit: "01",
        satuan: "mg/L",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parameter: "NH3N",
        range: randomNum(),
        unit: "01",
        satuan: "mg/L",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parameter: "TDS",
        range: randomNum(),
        unit: "01",
        satuan: "mg/L",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parameter: "BOD 5",
        range: randomNum(),
        unit: "01",
        satuan: "mg/L",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parameter: "COD",
        range: randomNum(),
        unit: "01",
        satuan: "mg/L",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parameter: "Fe 5",
        range: randomNum(),
        unit: "01",
        satuan: "mg/L",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parameter: "Cu",
        range: randomNum(),
        unit: "01",
        satuan: "mg/L",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        parameter: "Cr",
        range: randomNum(),
        unit: "01",
        satuan: "mg/L",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('parameters', null, {
      truncate:true,
      restartIdentity:true
    });
  },
};
